package com.kyrychenko;

public class Main {

    public static void main(String[] args) {
        int[] a = {1,2,2,2,10,15};
        int[] b = {2,7,14,35,8};

        int[] c = same(a, b);
        print(c);
        c = deleteSeries(a);
        print(c);
        c = delete(a);
        print(c);
        c = onlyInOne(a, b);
        print(c);
    }

    public static int[] same(int[] a, int[] b) {
        int[] c = new int[a.length];
        int k = 0;
        for (int i = 0; i < a.length ; i++) {
            for (int j = 0; j < b.length; j++) {
                if(a[i] == b[j]) {
                    k++;
                    c[i] = a[i];
                }
            }

        }
        return filter(c, k);
    }

    public static int[] onlyInOne(int[] a, int[] b) {
        int[] c = new int[a.length];
        int k = 0;
        int t = 0;
        for (int i = 0; i < a.length ; i++) {
            for (int j = 0; j < b.length; j++) {
                if(a[i] != b[j]) {
                    t++;
                    c[i] = a[i];
                }
            }
            if(t == b.length){
                k++;
                c[i] = a[i];
            }
            t = 0;
        }
        return filter(c, k);
    }

    public static int[] delete(int[] a) {
        int t = 0;
        int k = 0;
        int[] c = new int[a.length];
        for (int i = 0; i < a.length ; i++) {
            for (int j = 0; j < a.length; j++) {
                if(a[i] == a[j]) {
                    t++;
                    c[i] = a[i];
                }
            }
            if(t <= 2){
                k++;
                c[i] = a[i];
            }
            t = 0;
        }
        return filter(c, k);
    }

    public static int[] deleteSeries(int[] a) {
        int t = 0;
        int k = 0;
        int[] c = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            for (int j = i; j < a.length-1; j++) {
                if((a[i] != a[j]) && (t < 2)){
                    k++;
                    c[i] = a[i];
                    break;
                } else t++;
            }

            if(t >= 2){
                c[i] = a [i];
                i += t;
            }
            t = 0;
        }
        return filter(c, k);
    }

    public static int[] filter(int[] c, int length) {
        int[] p = new int[length];
        for (int i = 0; i < length ; i++) {
            p[i] = c[i];
        }
        return p;
    }

    public static void print(int[] c) {
        for (int i = 0; i < c.length; i++) {
            System.out.println(c[i]);
        }
        System.out.println(" -------------- ");
    }
}

